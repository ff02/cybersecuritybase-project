package sec.project;

import org.apache.catalina.Context;
import org.apache.catalina.SessionIdGenerator;
import org.apache.catalina.util.StandardSessionIdGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatContextCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;

@SpringBootApplication
public class CyberSecurityBaseProjectApplication implements EmbeddedServletContainerCustomizer {
    int sesid = 0;

    public static void main(String[] args) throws Throwable {
        SpringApplication.run(CyberSecurityBaseProjectApplication.class);
    }

    @Override
    public void customize(ConfigurableEmbeddedServletContainer cesc) {
        ((TomcatEmbeddedServletContainerFactory) cesc).addContextCustomizers(new TomcatContextCustomizer() {
            @Override
            public void customize(Context cntxt) {
                // Caused some red error on prod so we disabled it. We're unhackable anyways.
                cntxt.getManager().setSessionIdGenerator(sessionIdGenerator());
                cntxt.setUseHttpOnly(false);
                //cntxt.getManager().getSessionIdGenerator().setSessionIdLength(4);
                //System.out.println(cntxt.getManager().getSessionIdGenerator().getSessionIdLength() );
            }
            public SessionIdGenerator sessionIdGenerator() {
                return new StandardSessionIdGenerator() {
                    @Override
                    public String generateSessionId(String route) {
                        sesid++;
                        return Integer.toString(sesid);
                    }
                };
            }
                
            });
    }
}