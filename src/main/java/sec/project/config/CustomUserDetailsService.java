package sec.project.config;

import java.util.Arrays;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import sec.project.domain.Account;
import sec.project.repository.AccountRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private AccountRepository accountRepository;

    @PostConstruct
    public void init() {
        // testusers
        Account user = new Account();
        user.setName("ted");
        user.setIban("0000");
        // Set crypted password
        //user.setPassword("$2a$10$9tzllaABuNZ6OkXHCjfFh.jA1IeKL/fA1dNB9cx8XlJ44En/fi4MO");
        // or
        //set plain text password
        user.setPassword("letmein");
        user.setBalance(2147483647);
        user.setAddress("ted@nowhere.org");
        accountRepository.save(user);
            
        Account user2 = new Account();
        user2.setName("fred");
        user2.setIban("0001");
        // Set crypted password
        //user2.setPassword("$2a$10$9tzllaABuNZ6OkXHCjfFh.jA1IeKL/fA1dNB9cx8XlJ44En/fi4MO");
        // or
        //set plain text password
        user2.setPassword("letmein");
        user2.setBalance(1000);
        user2.setAddress("fred@nowhere.org");
        accountRepository.save(user2);

    }
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        System.out.println("Loaduser: " + username);
        System.out.flush();

        //Account account = accountRepository.findByUsername(username);
        Account user = accountRepository.findByName(username);
        if (user == null) {
            throw new UsernameNotFoundException("No such user: " + user);
        }
        
        return new org.springframework.security.core.userdetails.User(
                user.getName(),
                user.getPassword(),
                true,
                true,
                true,
                true,
                Arrays.asList(new SimpleGrantedAuthority("USER")));
    }
}
