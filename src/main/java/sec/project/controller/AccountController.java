package sec.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.PlaintextPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import sec.project.domain.Account;
import sec.project.repository.AccountRepository;

@Controller
public class AccountController {

    @Autowired
    private AccountRepository signupRepository;

    @Autowired
    // Plaintext passwords
    private PlaintextPasswordEncoder passwordEnc;       
    // crypted passwords
    //private PasswordEncoder passwordEnc;
    
    @RequestMapping(value = "/form", method = RequestMethod.GET)
    public String loadForm() {
        System.out.println("form get");
        return "form";
    }

    @RequestMapping(value = "/form", method = RequestMethod.POST)
    public String submitForm(@RequestParam String name, @RequestParam String password, @RequestParam String iban, @RequestParam String address) {
        System.out.println("form post");
        System.out.println("name   : " + name);
        System.out.println("passwd : " + password);
        System.out.println("iban   : " + iban);
        System.out.println("address: " + address);
        String encPasswd;

        // Plaintext passwords
        encPasswd = passwordEnc.encodePassword(password, null);
        // crypted passwords
        //encPasswd = passwordEnc.encode(password);
        System.out.println("encpass: " + encPasswd);
        
        Account user = new Account();
        user.setName(name);
        user.setPassword(encPasswd);
        user.setAddress(address);
        user.setIban(iban);
        user.setBalance(1000);
        signupRepository.save(user);
        return "done";
    }
    
    @RequestMapping(value = "/signups", method = RequestMethod.GET)
    public String view(Model model) {
        model.addAttribute("signups", signupRepository.findAll());
        return "signups";
    }
}
