package sec.project.controller;

import java.sql.Array;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import sec.project.repository.AccountRepository;
import sec.project.domain.Account;
import java.security.Principal;

@Controller
public class BankingController {

    @Autowired
    private AccountRepository accountRepository;

    @RequestMapping(value = "/transfer", method = RequestMethod.GET)
    public String list(Model model, Principal principal) {
        System.out.println("transfer get");
        System.out.flush();
        model.addAttribute("curaccount", accountRepository.findByName(principal.getName()));
        return "transfer";
    }

    @RequestMapping(value = "/transfer", method = RequestMethod.POST)
    public String transfer(@RequestParam String from, @RequestParam String to, @RequestParam Integer amount) {
        System.out.println("transfer post");
        System.out.flush();
        //try {
            Account accountFrom = accountRepository.findByIban(from);
            Account accountTo = accountRepository.findByIban(to);
            int sourceBalance = accountFrom.getBalance();
            int destBalance = accountTo.getBalance();
            
            if ( amount < 0 ||  amount > 2147483647 ) {
                // amount negative = "reverse" transfer
                // cant exceed max integer size
                return "redirect:/transfer";
            }
            if ( sourceBalance - amount < 0 ) {
                // Insufficient funds
                return "redirect:/transfer";
            }
            
            Long ldestBalance = new Long(destBalance);
            Long lamount = new Long(amount);
            Long lbalance = ldestBalance + lamount;
            //System.out.print("Balance   : " + ldestBalance.toString());
            //System.out.print("Amount    : " + lamount.toString());
            //System.out.print("New balace: " + lbalance.toString());
            if (ldestBalance + lamount > 2147483647) {
                // account would "overflow"
                System.out.print("Transfer does not fit in account. Heh.");
                return "redirect:/transfer";  
            }
                    
            accountFrom.setBalance(accountFrom.getBalance() - amount);
            accountTo.setBalance(accountTo.getBalance() + amount);

            accountRepository.save(accountFrom);
            accountRepository.save(accountTo);
/*
        }
        catch (NullPointerException e) {
            // Handle ExceptionType1 exceptions      
            System.out.print("Exception no such account");
        }
            */
    return "redirect:/transfer";
    }
    
    @RequestMapping(value = "/accountsearch", method = RequestMethod.GET)
    public String search() {
        System.out.println("search get");
        System.out.flush();
        return "accountsearch";
    }
    
    @RequestMapping(value = "/accountsearch", method = RequestMethod.POST)
    public String search(Model model, @RequestParam String searchString) {
        System.out.println("search post");
        System.out.flush();
        
        ArrayList accList = new ArrayList();
        try {
            Class.forName("org.h2.Driver");
            //Connection conn = DriverManager.getConnection("jdbc:h2:~/test", "sa", "");
            Connection conn = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "");
            Statement stmnt = conn.createStatement();
            //String sql = "SELECT * FROM account;";
            String sql = "SELECT name,iban,address FROM account WHERE name='" + searchString + "';";
            System.out.println(sql);

            ResultSet rs = stmnt.executeQuery(sql);
            /*
            System.out.println(rs.toString());
            Array rsarray = rs.getArray(1);
            System.out.println(rsarray.toString());
            System.out.flush();
            */
            /*
            ResultSetMetaData rsmd = rs.getMetaData();
            int columnsNumber = rsmd.getColumnCount();
            while (rs.next()) {
                for (int i = 1; i <= columnsNumber; i++) {
                    if (i > 1) System.out.print(",  ");
                    String columnValue = rs.getString(i);
                    System.out.print(columnValue + " " + rsmd.getColumnName(i));
                }
                System.out.println("");
            }
            */
            
            while (rs.next()) {
                String name = rs.getString("name");
                String iban = rs.getString("iban");
                String address = rs.getString("address");
                Account acc = new Account();
                acc.setName(name);
                acc.setIban(iban);
                acc.setAddress(address);
                accList.add(acc);
            }
            conn.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        /*
        if (accList.isEmpty()) {
            Account acc = new Account();
            acc.setName("No results found");
            acc.setIban("");
            accList.add(acc);
        }*/
        model.addAttribute("accounts", accList);
        System.out.println(accList.toString());
        return "accountsearch";
        
    }
}
