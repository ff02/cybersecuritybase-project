package sec.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class DefaultController {

    @Autowired
    private UserDetailsService userDetailsService; 

    @RequestMapping("/")
    public String main() {
        return "redirect:/login";
    } 
    
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        System.out.println("Login get");
        return "login";
    }
    
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String logged(@RequestParam String username, @RequestParam String password, @RequestParam String _csrf) {
        System.out.println("Logged post");
        System.out.flush();
        return "done";
    }
/*
    @RequestMapping("/main")
    public String main() {
        return "main";
    }
*/    
    @RequestMapping("/logout")
    public String logout() {
        return "logout";
    }

}
