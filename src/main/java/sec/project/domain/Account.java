package sec.project.domain;

import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.Id;
import org.springframework.data.jpa.domain.AbstractPersistable;
import org.springframework.security.authentication.encoding.PlaintextPasswordEncoder;

@Entity
public class Account extends AbstractPersistable<Long> {

    @Id
    private Long id;
    
    //@Column(unique = true)
    private String name;
    private String password;
    private String address;
    private String iban;
    private Integer balance;


    //private PlaintextPasswordEncoder pEnc;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getPassword() {
        return password;
    }

    public void setPassword(String passwd) {
        this.password = passwd;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }
    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

}
